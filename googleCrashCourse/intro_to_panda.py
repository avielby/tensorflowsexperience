import pandas as pd
pd.__version__

city_names = pd.Series(['San Francisco', 'San Jose', 'Sacramento'])
population = pd.Series([852469, 1015785, 485199])

cities = pd.DataFrame({ 'City name': city_names, 'Population': population })


cities['Area square miles'] = pd.Series([46.87, 176.53, 97.92])
cities['Population density'] = cities['Population'] / cities['Area square miles']

# Exercise 1:

cities['wide and saint name'] = pd.Series([1 if x.startswith('San') and y>50 else 0 for x in cities['City name'] for y in cities['Area square miles'] ])

print(cities)